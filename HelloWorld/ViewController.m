//
//  ViewController.m
//  HelloWorld
//
//  Created by Nicholas Ryan Seney on 1/19/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

int count = 4;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)handleButtonClick:(id)myButton
{
   
    switch (count%4) {
        case 0:
            _myLabel.text = @"Nick Seney";
            break;
            
        case 1:
            _myLabel.text = @"Great Memes";
            break;
        case 2:
            _myLabel.text = @"Keep it up!";
            break;
        default:
            _myLabel.text = @"Great Job!";
            break;
    }
     count++;
}



@end
