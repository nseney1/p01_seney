//
//  ViewController.h
//  HelloWorld
//
//  Created by Nicholas Ryan Seney on 1/19/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic,retain) IBOutlet UILabel *myLabel;
@property (nonatomic,retain) IBOutlet UIButton *myButton;

- (IBAction)handleButtonClick:(id)myButton;


@end

